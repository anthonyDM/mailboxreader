<?php
// error_reporting(E_ALL);
// ini_set('display_errors', '1');

require_once __DIR__.'/../vendor/autoload.php';

$app = new Silex\Application();

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

$app->register(new Silex\Provider\TranslationServiceProvider());
$app->register(new Silex\Provider\ValidatorServiceProvider());
$app->register(new Silex\Provider\FormServiceProvider());
$app->register(new Anthony\ImapMailbox());
$app->register(new Silex\Provider\SessionServiceProvider());
$app->register(new Silex\Provider\TwigServiceProvider(), array(
    	'twig.path' => __DIR__.'/../view'
    ));

$app->before(function ($request) {
    $request->getSession()->start();
});

$app->match('/readEmail', function(Request $request) use($app) {

	$email = $request->get('form')['email'];
	$password = $request->get('form')['password'];

	$app['session']->set('userinfo', array(
			'email' => $email,
			'password' => $password
		));

	$app['session']->save();

	$app['mailbox']->connect('{imap.gmail.com:993/imap/ssl}INBOX', $email, $password);

	if ($mailsIds = $app['mailbox']->searchMailBox('ALL')) {
		
		$a = array();
		foreach ($mailsIds as $id) {
			$a[] = $app['mailbox']->getMail($id);
		}

		return $app['twig']->render('mailbox.twig', array(
			'a' => $a,
			'email' => $email
		));

	} else {

		$app['session']->set('failedLoginMessage', 'Some information you have entered is incorrect');
		$app['session']->save();

		return $app->redirect('login');
	}    
});

$app->match('/login', function(Request $request) use($app) {

    $form = $app['form.factory']->createBuilder('form', $data = null)
        ->add('password', 'password')
   		->add('email', 'text')
        ->getForm();

	$form->handleRequest($request);

    return $app['twig']->render('login.twig', array(
    	'form' => $form->createView()
    ));
});

$app->get('/mailDetail/{id}', function($id) use($app) {

	$email = $app['session']->get('userinfo')['email'];
	$password = $app['session']->get('userinfo')['password'];
	$app['mailbox']->connect('{imap.gmail.com:993/imap/ssl}INBOX', $email, $password);

	$mail = $app['mailbox']->getMail($id);

	return $app['twig']->render('maildetail.twig', array(
		'mail' => $mail,
	));
});

$app->run();
